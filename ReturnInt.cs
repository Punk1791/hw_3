using System;

namespace Solid
{
    /// <summary>
    /// Класс для возвращения сгенерируемого числа
    /// </summary>
    public class ReturnInt : ReturnGenerateNumber
    {
        public ReturnInt(int _at, int _to) : base(_at, _to)
        {
            base.Generator = new GenerateInt();
            base.at = _at;
            base.to = _to;
        }
        public override object ReturnNumber()
        {
            return base.ReturnNumber();
        }
    }
}