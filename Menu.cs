using System;

namespace Solid
{
    public class GameWrapper : IGameWrapper
    {
        private ISettings Settings;
        public bool isGame = true;
        public void Start()
        {
            Console.WriteLine("Введите количество попыток:");
            Settings.CountAttempts = Convert.ToInt16(Console.ReadLine());

            Console.WriteLine("Введите начальное число:");
            Settings.At = Convert.ToInt16(Console.ReadLine());

            Console.WriteLine("Введите конечное число:");
            Settings.To = Convert.ToInt16(Console.ReadLine());

            Console.WriteLine("Введите загадываемое число:");
            var Number = Convert.ToInt16(Console.ReadLine());

            var Game = new Logic(Settings);

            for (int i = 0; i < Settings.CountAttempts; i++)
            {
                var NewNumber = Game.ReturnGeneratingNumber(Number);
                if (NewNumber == Number)
                {
                    Console.WriteLine("Повезло!");
                }
                else
                {
                    if (NewNumber - Number > 0) Console.WriteLine("Не повезло! Ваше число меньше отгадываемого");
                    else Console.WriteLine("Не повезло! Ваше число больше отгадываемого");
                }
            }
        }

        public void End()
        {
            isGame = false;
        }
    }
}