using System;

namespace Solid
{
    /// <summary>
    /// Класс для возвращения сгенерируемого числа
    /// </summary>
    public class ReturnGenerateNumber
    {
        public IGenerate Generator { get; set;}
        public int at { get; set;}
        public int to { get; set;}
        public ReturnGenerateNumber(int _at, int _to)
        {
            Generator = new IGenerate();
            at = _at;
            to = _to;
        }
        public virtual object ReturnNumber()
        {
            Generator.Generate(at, to);
            return Generator.Number;
        }
    }
}