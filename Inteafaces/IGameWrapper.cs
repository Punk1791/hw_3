using System;

namespace Solid
{
    /// <summary>
    /// Интерфейс для обертки игры
    /// </summary>
    public interface IGameWrapper
    {
        public void Start() { }
        public void End() { }
    }
}