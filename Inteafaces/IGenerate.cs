using System;

namespace Solid
{
    /// <summary>
    /// Интерфейс для генератора
    /// </summary>
    public class IGenerate
    {
        public object Number { get;}
        public virtual void Generate(int at, int to)
        {
        }
    }
}