﻿using System;

namespace Solid
{
    public static class Program
    {
        public static void Main(string[] args)
        {
            try
            {
                var Gaming = new GameWrapper();
                while (Gaming.isGame)
                {
                    Console.WriteLine("Играем (y/n)?");
                    var choise = Console.ReadLine();
                    if (choise == "y")
                    {
                        Gaming.Start();
                    }
                    else
                    {
                        Gaming.End();
                    }
                }
            }
            catch
            {

            }
        }
    }
}