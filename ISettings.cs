using System;

namespace Solid
{
    public interface ISettings
    {
        public int CountAttempts { get; set; }
        public int At { get; set;}
        public int To { get; set;}
        public bool IsFinished { get; set;}
    }
}