using System;

namespace Solid
{
    /// <summary>
    /// Класс для логики игры - проверки числа
    /// </summary>
    public class Logic
    {
        private ISettings Settings { get; }

        public Logic(ISettings ThisSettings)
        {
            Settings = ThisSettings;
        }

        public int ReturnGeneratingNumber(int number)
        {
            try
            {
                if (number<Settings.At || number>Settings.To) throw new Exception();
                var Generate = new ReturnInt(Settings.At, Settings.To);
                return (int)Generate.ReturnNumber();
            }
            catch
            {
                Console.WriteLine("Ошибка");
                return new int();
            }
        }
    }
}