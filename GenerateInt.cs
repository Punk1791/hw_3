using System;

namespace Solid
{
    /// <summary>
    /// Генератор числа типа int
    /// </summary>
    public class GenerateInt : IGenerate
    {
        public int Number { get; set; }
        public override void Generate(int at, int to)
        {
            Number = new Random().Next(at,to);
        }
    }
}